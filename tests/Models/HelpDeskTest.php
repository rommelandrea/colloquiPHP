<?php
/**
 * User: Andrea Romanello aka rommel
 */

namespace MyAppTest\Models;

use Faker\Factory;
use MyApp\Models\HelpDesk;
use PHPUnit\Framework\TestCase;

class HelpDeskTest extends TestCase
{
    protected $faker;

    protected function setUp()
    {
        $this->faker = Factory::create();
    }

    public function test__construct()
    {
        $name = $this->faker->company;
        $h = new HelpDesk($name);
        $this->assertEquals(0, strcasecmp($name, $h->getName()));
    }

    public function testIncreaseAllSalary()
    {
        $h = new HelpDesk($this->faker->company);
        $h->addEmployee($this->faker->firstName, $this->faker->jobTitle);
        $h->addEmployee($this->faker->firstName, $this->faker->jobTitle);
        $h->addEmployee($this->faker->firstName, $this->faker->jobTitle);

        $this->assertNotEmpty($h->getEmployees());

        $h->increaseAllSalary();

        foreach ($h->getEmployees() as $employee) {
            $this->assertEquals(1100, $employee->getSalary());
        }

    }
}
