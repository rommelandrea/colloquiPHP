#Test PHP
All'interno di questo progetto sono presenti due cartelle, la cartella src dove ci sono i sorgenti del progetto la cartella tests dove ci sono gli unittest

Per completare l'esercizio, bisogna correggere i metodi, completare quelli incompleti presenti all'interno della cartella src.
Sono a disposizione gli unittest che verificano il corretto funzionamento del codice, ed è presente anche un file index con degli esempi di che possono aiutare a sviluppare.

ATTENZIONE non è possibile cambiare la visibilità delle variabili al fine di far funzionare il codice

Per riuscire a fare funzionare e testare il progetto bisogna avere installato sul proprio PC php >= 7.0 e composer.
Una volta installati i requisiti, bisogna lancaire il comando `composer install`

Una volta fatto questo per lanciare i test, se non si ha phpunit installato sulla macchina si può lanciare `vendor/bin/phpunit`

E' richiesto di commentare le soluzioni e nel caso della funzione di sort anche motivando le scelte.
