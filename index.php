<?php
require 'vendor/autoload.php';

use Faker\Factory;
use MyApp\Models\Customer;
use MyApp\Models\HelpDesk;

$faker = Factory::create();

$customer = new Customer($faker->company);

d($customer);

$customer->addOffice($faker->name, $faker->address, 'laboratorio');

$customer->addEmployee($faker->name, $faker->jobTitle);
$customer->addEmployee($faker->name, $faker->jobTitle);
$customer->addEmployee($faker->name, $faker->jobTitle);

d($customer);

$helpDesk = new HelpDesk($faker->company);
$helpDesk->addEmployee($faker->name, $faker->jobTitle);
$helpDesk->addEmployee($faker->name, $faker->jobTitle);
$helpDesk->addEmployee($faker->name, $faker->jobTitle);
$helpDesk->addOffice($faker->name, $faker->address, 'laboratorio');

d($helpDesk);

$helpDesk->increaseAllSalary();

d($helpDesk);
